**Mediacurrent**

Component-Based Development with Drupal
=

This is a public repository with starter files for Mediacurrent's component based training. Attendees can get a Drupal site up and running quickly ahead of a live or online session with a Mediacurrent trainer.

### What is this repository for? ###

* Follow the steps below to get your local Drupal site set up.
### Developers will need: ###

- [ ] [Docker][docker link] 
- [ ] Composer v2: [Mac][composer mac] | [Windows][composer windows] 
- [ ] [DDEV][ddev link]
- [ ] NVM (Node Version Manager): [Mac][nvm link mac] | [Windows][nvm link windows]

[docker link]: https://docs.docker.com/get-docker
[composer mac]: https://pilsniak.com/install-composer-mac-os
[composer windows]: https://devanswers.co/install-composer-php-windows-10/
[ddev link]: https://ddev.readthedocs.io/en/latest/
[nvm link mac]: https://tecadmin.net/install-nvm-macos-with-homebrew/
[nvm link windows]: https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-windows

Do not proceed further without installing the dependencies listed above. 
***

### Clone this repository

Clone this repo:
  ```
  git clone git@bitbucket.org:mediacurrent/mc_training_starter.git mc_training
  ```
Change to the new directory:
  ```
  cd mc_training
  ```
Checkout the starter branch:
  ```
  git checkout portland
  ```

### Initialize codebase
Run the following command to initialize the codebase. This runs composer install.  As this is the first time being run, it is a composer update and calculates all dependencies.
  ```
  composer install
  ```

### Configure DDEV
Run DDEV's configuration tool. You can pass in defaults (recommended options below), or you can run it without arguments for an interactive configuration tool.
```
ddev config --docroot=web --project-name="mc-training" --project-type=drupal9 --webserver-type="nginx-fpm" --create-docroot
```
Or manually
```
Project name: mc-training
Docroot Location = web
Project Type = drupal9
```
### Start DDEV
After configuration has been completed, start the ddev containers.
```
ddev start
```
You should see something like this in your terminal:
![Successfully started mc-training logging](startddev.png)
In your browser go to: https://mc-training.ddev.site (or click on the link in your terminal)


### Import provide database
We have provided a bare bones database to get you started. Import using the following command.
```
ddev import-db --src=portland.db.sql.gz
```
***
## Having trouble?

* Training attendees should receive and email in advance. In it we provide a date/time for online help. Please refer to that email.
* Or reach out to us at mediacurrent.com
